package Container;
import java.util.Comparator;

public class CountryComparator<T> implements Comparator<Car> {

	@Override
	public int compare(Car o1, Car o2) {
		return o1.getCountry().compareTo(o2.getCountry());
	}
}

