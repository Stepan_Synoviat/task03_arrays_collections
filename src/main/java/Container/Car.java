package Container;

import java.util.Random;

public class Car {
	private String brand;
	private String country;

	public Car() {

	}

	public Car(String brand, String country) {
		this.brand = brand;
		this.country = country;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public void setAll(Car o1) {
		this.brand = o1.brand;
		this.country = o1.country;
	}

	@Override
	public String toString() {
		return " Brand: "
				+ brand
				+ "Country: "
				+ country;
	}

	public static Car getCar() {

		Random rnd = new Random();
		final int lim = 5;
		int choose = rnd.nextInt(lim);

		Car car = new Car();
		switch (choose) {
			case 0:
				car.setCountry("USA");
				car.setBrand("FORD");
				break;
			case 1:
				car.setCountry("Germany");
				car.setBrand("BMW");
				break;
			case 2:
				car.setCountry("England");
				car.setBrand("Mini");
				break;
			case 3:
				car.setCountry("China");
				car.setBrand("Sang-Yang");
				break;
			case 4:
				car.setCountry("Japan");
				car.setBrand("Mazda");
				break;
			case 5:
				car.setCountry("Sweden");
				car.setBrand("Volvo");
				break;
		}
		return car;
	}
}
