package Container;


import java.util.Arrays;

public class Main {
	public static void main(String[] args) {
		Car[] list = new Car[5];
		for (int i = 0; i < 5; i++) {
			list[i] = (Car.getCar());
		}
		System.out.println("Before sort :");
		for (Car c : list) {
			System.out.println(c.getCountry());
		}
		Arrays.sort(list, new CountryComparator());
		System.out.println("country sort :");
		for (Car c : list) {
			System.out.println(c.getCountry());
		}
		System.out.println("Brand sort :");
		Arrays.sort(list, new BrandComparator());
		for (Car c : list) {
			System.out.println(c.getBrand());
		}
	}

}

