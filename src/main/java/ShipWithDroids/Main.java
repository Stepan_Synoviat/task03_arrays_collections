package ShipWithDroids;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.PriorityQueue;

public class Main {
	public static void main(String[] args) {
		Droid d1 = new Droid("R2D2", 123);
		Droid d2 = new Droid("R2D3", 124);
		Droid d3 = new Droid("R2D4", 125);
		Droid d4 = new Droid("R2D5", 126);

		Ship ship = new Ship("Argo");
		ship.setDroid(d1);
		ship.setDroid(d2);
		ship.setDroid(d3);
		ship.setDroid(d4);

		for (Droid droid:ship.getList() ){
		System.out.println(droid.getName());
		}

		DroidPriorityQueue droidPriorityQueue = new DroidPriorityQueue();

		droidPriorityQueue.setToQueue(d4);
		droidPriorityQueue.setToQueue(d3);
		droidPriorityQueue.setToQueue(d2);
		droidPriorityQueue.setToQueue(d1);

		for (Droid droid: droidPriorityQueue.getQueue()) {
			System.out.println(droid.getId());
		}








	}
}
