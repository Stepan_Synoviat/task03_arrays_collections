package ShipWithDroids;

import java.util.ArrayList;
import java.util.Comparator;

public class Ship {
	private String name;
	private ArrayList<Droid> list = new ArrayList<>();

	public Ship(String name) {
		this.name = name;
	}

	public ArrayList<Droid> getList() {
		return this.list;
	}

	public void setDroid(Droid droid) {
		this.list.add(droid);
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	//public static Comparator<Droid> idComparator = (c1, c2) -> (int) (c1.getId() - c2.getId());
}
