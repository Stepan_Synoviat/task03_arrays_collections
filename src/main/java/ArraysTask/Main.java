package ArraysTask;


import java.util.Random;

public class Main {
	public static void main(String[] args) {
		int[] a = Main.getArray(5);
		int[] b = Main.getArray(5);
		int[] c = addArrays(a, b);
		for (int i: c) {
			System.out.print(i+" ");
		}
		System.out.println(" ");

		int [] d = getClone(a);
		for (int u: d) {
			System.out.print(u + " ");
		}
		System.out.println(" ");

		noDups(c);
	}
	// create a new array
	private static int[] getArray(int size) {
		Random random = new Random();
		int[] array = new int[size];
		for (int i = 0; i < array.length; i++) {
			array[i] = random.nextInt(5);
		}
		return array;
	}

	// add two arrays to another one
	private static int[] addArrays(int[] arr1, int[] arr2) {
		int lenght = arr1.length + arr2.length;
		int[] newArray = new int[lenght];
		int temp = 0;
		for (int i = 0; i < arr1.length; i++) {
			newArray[temp] = arr1[i];
			temp++;
		}
		for (int j = 0; j < arr2.length; j++) {
			newArray[temp] = arr2[j];
			temp++;
		}
		return newArray;
	}

	// Delete duplicates from array
	public static int[] noDups(int[] a) {
		int n = a.length;
		for (
				int i = 0, m = 0;
				i != n; i++, n = m) {
			for (int j = m = i + 1; j != n; j++) {
				if (a[j] != a[i]) {
					if (m != j) a[m] = a[j];
					m++;
				}
			}
		}
		if (n != a.length) {
			int[] b = new int[n];
			for (int i = 0; i < n; i++) b[i] = a[i];
			a = b;
		}
		for (
				int x : a)
			System.out.print(x + " ");
		System.out.println();
		return a;
	}
	//create a cloned array
	public static int[] getClone(int[] original){
		int[]clone= original.clone();
		return clone;
	}

}
